var group___tasks =
[
    [ "EZB_Aktivierung", "group___tasks.html#ga8054a513760552caeea946e439a50461", null ],
    [ "EZB_Blockiert", "group___tasks.html#ga622c17f8ea689b589209a53df2da6255", null ],
    [ "EZB_CreateTask", "group___tasks.html#gafdc2a179445236401fb0d5f1473795e7", null ],
    [ "EZB_Deaktivierung", "group___tasks.html#ga32c40da9d635efd1b8e20dd82adf59bb", null ],
    [ "EZB_Init", "group___tasks.html#gad25f01f454c9cee8aabf43b9cff1946a", null ],
    [ "EZB_Laufend", "group___tasks.html#gabc5c9b1bd2789eff6fb5154fda66d90e", null ],
    [ "EZB_Suche", "group___tasks.html#ga67fd658b2c8c2ad0101b970d477ba346", null ],
    [ "STACK_SETUP", "group___tasks.html#ga2d5ff492903b7f843fbea8343d061813", null ],
    [ "CurrentTVB", "group___tasks.html#ga454a19081df3d0802777d652aaae993e", null ],
    [ "NewTVB", "group___tasks.html#gaf2e6e144296b9c896c78c665ed879202", null ],
    [ "rdy_count", "group___tasks.html#gaf831e8eb14053d307bab557a804711a1", null ],
    [ "rdytsk", "group___tasks.html#gabad95ae9c6e02bb05960026beea9bc8b", null ],
    [ "run", "group___tasks.html#gafbd70d607f110b7d832d1162882734db", null ],
    [ "run_next", "group___tasks.html#ga604ea01764bc13f6bcd94943e46e7262", null ],
    [ "SPA", "group___tasks.html#ga14adfcf7cb15ba7c3b426200fe887274", null ],
    [ "SPB", "group___tasks.html#ga78ac96a7e5808ef19f1832ad41c21e13", null ],
    [ "tasks_created", "group___tasks.html#ga43055a1eff48def91573570c195ffac4", null ],
    [ "time_now", "group___tasks.html#ga2cf6d2fe3c1046d7c01123c1524df502", null ],
    [ "TVB_use", "group___tasks.html#gacbf44c62b572d988757d43c443c7ae55", null ],
    [ "TVB_use1", "group___tasks.html#ga88a0005896af7b22149d561c541ab612", null ]
];