var struct_t_v_b =
[
    [ "function", "struct_t_v_b.html#a0bd084ac2d6295338fa17d35371990b9", null ],
    [ "noend", "struct_t_v_b.html#a8591729cfc732ba517f67bce24d59cca", null ],
    [ "prio", "struct_t_v_b.html#acc0b27a6740f03639727be452f1e6b83", null ],
    [ "slice_reached", "struct_t_v_b.html#ac8c875d55100b125cb481443c2e87361", null ],
    [ "stack_pointer", "struct_t_v_b.html#a99811bc2de2bfaf3aff517a2cb53d4fa", null ],
    [ "stack_top", "struct_t_v_b.html#a2921a7fd5d09300564a6a5835c5613fe", null ],
    [ "state", "struct_t_v_b.html#a64889a7695b77a1c296e2efb2f628a40", null ],
    [ "time_p", "struct_t_v_b.html#a8288f89302f663b13913832a61ffd9a3", null ],
    [ "time_r", "struct_t_v_b.html#abdfa5c72f8cd38bf45214cf04bb36740", null ],
    [ "time_w", "struct_t_v_b.html#a04e0c2d6a42df116d6d259fd24454aaa", null ]
];