var group___prozessor =
[
    [ "EZB_ExitTask", "group___prozessor.html#gab964a1b33d971765383f84f1c82a53a3", null ],
    [ "EZB_ExitTask2", "group___prozessor.html#ga31827d51261bb46ff144fa1831dcd49d", null ],
    [ "EZB_StartTask", "group___prozessor.html#gacf158687655211229695fc785fca5ec9", null ],
    [ "EZB_StartTask_Internals", "group___prozessor.html#ga23d07ac5dbbe8e72c6a72b9b7062bb7a", null ],
    [ "EZB_SystemStart", "group___prozessor.html#gae407ac0a1f68cc359350e327883a88ab", null ],
    [ "EZB_Wait", "group___prozessor.html#ga4e9a9a8418088ccb5ac5a082126f4902", null ],
    [ "EZB_Wait_Internals", "group___prozessor.html#ga248b469b381e91f06142af5378859516", null ]
];